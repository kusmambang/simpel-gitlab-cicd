package main

import (
	"github.com/julienschmidt/httprouter"
	"github.com/stretchr/testify/assert"
	"io"
	"net/http/httptest"
	"testing"
)

func TestIndex(t *testing.T) {
	router := httprouter.New()
	router.GET("/", Index)

	request := httptest.NewRequest("GET", "http://localhost:9191/", nil)
	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()
	body, _ := io.ReadAll(response.Body)

	assert.Equal(t, "Welcome!", string(body))
}
