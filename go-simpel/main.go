package main

import (
	"github.com/julienschmidt/httprouter"
	"log"
	"net/http"
)

func main() {
	router := httprouter.New()
	router.GET("/", Index)

	log.Println("Running on port 9191")
	log.Fatal(http.ListenAndServe(":9191", router))
}
